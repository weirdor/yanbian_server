package org.jeecg.modules.edu.pubBanner.service;

import org.jeecg.modules.edu.pubBanner.entity.PubBanner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 轮播图表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface IPubBannerService extends IService<PubBanner> {

}
