package org.jeecg.modules.edu.umsClassroomRule.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.umsClassroomRule.entity.UmsClassroomRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 教室规则
 * @Author: jeecg-boot
 * @Date:   2021-12-12
 * @Version: V1.0
 */
public interface UmsClassroomRuleMapper extends BaseMapper<UmsClassroomRule> {

}
