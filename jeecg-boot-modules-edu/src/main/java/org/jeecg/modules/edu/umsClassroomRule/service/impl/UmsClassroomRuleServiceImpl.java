package org.jeecg.modules.edu.umsClassroomRule.service.impl;

import org.jeecg.modules.edu.umsClassroomRule.entity.UmsClassroomRule;
import org.jeecg.modules.edu.umsClassroomRule.mapper.UmsClassroomRuleMapper;
import org.jeecg.modules.edu.umsClassroomRule.service.IUmsClassroomRuleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 教室规则
 * @Author: jeecg-boot
 * @Date:   2021-12-12
 * @Version: V1.0
 */
@Service
public class UmsClassroomRuleServiceImpl extends ServiceImpl<UmsClassroomRuleMapper, UmsClassroomRule> implements IUmsClassroomRuleService {

}
