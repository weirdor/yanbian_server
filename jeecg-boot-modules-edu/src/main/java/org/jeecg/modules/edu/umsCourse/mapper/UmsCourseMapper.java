package org.jeecg.modules.edu.umsCourse.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.umsCourse.entity.UmsCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 课程表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface UmsCourseMapper extends BaseMapper<UmsCourse> {

}
