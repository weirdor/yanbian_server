package org.jeecg.modules.edu.umsUser.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.edu.umsUser.entity.UmsUser;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.edu.umsUser.parm.LoginParam;

/**
 * @Description: 用户表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface IUmsUserService extends IService<UmsUser> {

    /**
     * 登陆
     *
     * @param loginParam
     * @return
     */
    UmsUser loginAuth(LoginParam loginParam);

    Result checkUserIsEffective(UmsUser umsUser);

}
