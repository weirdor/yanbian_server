package org.jeecg.modules.edu.umsTeachingPlan.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsTeachingPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 教学计划
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface UmsTeachingPlanMapper extends BaseMapper<UmsTeachingPlan> {

}
