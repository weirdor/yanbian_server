package org.jeecg.modules.edu.pubBanner.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.pubBanner.entity.PubBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 轮播图表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface PubBannerMapper extends BaseMapper<PubBanner> {

}
