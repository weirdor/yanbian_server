package org.jeecg.modules.edu.umsClassroomRule.service;

import org.jeecg.modules.edu.umsClassroomRule.entity.UmsClassroomRule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 教室规则
 * @Author: jeecg-boot
 * @Date:   2021-12-12
 * @Version: V1.0
 */
public interface IUmsClassroomRuleService extends IService<UmsClassroomRule> {

}
