package org.jeecg.modules.edu.umsTeachingPlan.service.impl;

import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsTeachingPlan;
import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsCourseRecord;
import org.jeecg.modules.edu.umsTeachingPlan.mapper.UmsCourseRecordMapper;
import org.jeecg.modules.edu.umsTeachingPlan.mapper.UmsTeachingPlanMapper;
import org.jeecg.modules.edu.umsTeachingPlan.service.IUmsTeachingPlanService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 教学计划
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class UmsTeachingPlanServiceImpl extends ServiceImpl<UmsTeachingPlanMapper, UmsTeachingPlan> implements IUmsTeachingPlanService {

	@Autowired
	private UmsTeachingPlanMapper umsTeachingPlanMapper;
	@Autowired
	private UmsCourseRecordMapper umsCourseRecordMapper;
	
	@Override
	@Transactional
	public void saveMain(UmsTeachingPlan umsTeachingPlan, List<UmsCourseRecord> umsCourseRecordList) {
		umsTeachingPlanMapper.insert(umsTeachingPlan);
		if(umsCourseRecordList!=null && umsCourseRecordList.size()>0) {
			for(UmsCourseRecord entity:umsCourseRecordList) {
				//外键设置
				entity.setPlanId(umsTeachingPlan.getId());
				umsCourseRecordMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(UmsTeachingPlan umsTeachingPlan,List<UmsCourseRecord> umsCourseRecordList) {
		umsTeachingPlanMapper.updateById(umsTeachingPlan);
		
		//1.先删除子表数据
		umsCourseRecordMapper.deleteByMainId(umsTeachingPlan.getId());
		
		//2.子表数据重新插入
		if(umsCourseRecordList!=null && umsCourseRecordList.size()>0) {
			for(UmsCourseRecord entity:umsCourseRecordList) {
				//外键设置
				entity.setPlanId(umsTeachingPlan.getId());
				umsCourseRecordMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		umsCourseRecordMapper.deleteByMainId(id);
		umsTeachingPlanMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			umsCourseRecordMapper.deleteByMainId(id.toString());
			umsTeachingPlanMapper.deleteById(id);
		}
	}
	
}
