package org.jeecg.modules.edu.pubBanner.service.impl;

import org.jeecg.modules.edu.pubBanner.entity.PubBanner;
import org.jeecg.modules.edu.pubBanner.mapper.PubBannerMapper;
import org.jeecg.modules.edu.pubBanner.service.IPubBannerService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 轮播图表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class PubBannerServiceImpl extends ServiceImpl<PubBannerMapper, PubBanner> implements IPubBannerService {

}
