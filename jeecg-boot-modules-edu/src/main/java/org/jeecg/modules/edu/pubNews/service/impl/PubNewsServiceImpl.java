package org.jeecg.modules.edu.pubNews.service.impl;

import org.jeecg.modules.edu.pubNews.entity.PubNews;
import org.jeecg.modules.edu.pubNews.mapper.PubNewsMapper;
import org.jeecg.modules.edu.pubNews.service.IPubNewsService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 新闻表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class PubNewsServiceImpl extends ServiceImpl<PubNewsMapper, PubNews> implements IPubNewsService {

}
