package org.jeecg.modules.edu.pubNews.service;

import org.jeecg.modules.edu.pubNews.entity.PubNews;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 新闻表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface IPubNewsService extends IService<PubNews> {

}
