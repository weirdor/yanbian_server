package org.jeecg.modules.edu.umsStudent.service.impl;

import org.jeecg.modules.edu.umsStudent.entity.UmsStudent;
import org.jeecg.modules.edu.umsStudent.mapper.UmsStudentMapper;
import org.jeecg.modules.edu.umsStudent.service.IUmsStudentService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 学生表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class UmsStudentServiceImpl extends ServiceImpl<UmsStudentMapper, UmsStudent> implements IUmsStudentService {

}
