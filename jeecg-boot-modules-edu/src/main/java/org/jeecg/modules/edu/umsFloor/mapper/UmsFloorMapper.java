package org.jeecg.modules.edu.umsFloor.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.umsFloor.entity.UmsFloor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 教学楼
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
public interface UmsFloorMapper extends BaseMapper<UmsFloor> {

}
