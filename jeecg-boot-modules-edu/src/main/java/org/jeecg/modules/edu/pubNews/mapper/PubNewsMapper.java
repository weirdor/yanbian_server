package org.jeecg.modules.edu.pubNews.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.pubNews.entity.PubNews;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 新闻表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface PubNewsMapper extends BaseMapper<PubNews> {

}
