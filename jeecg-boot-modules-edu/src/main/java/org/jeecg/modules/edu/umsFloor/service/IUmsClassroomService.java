package org.jeecg.modules.edu.umsFloor.service;

import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 教室表
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
public interface IUmsClassroomService extends IService<UmsClassroom> {

	public List<UmsClassroom> selectByMainId(String mainId);
}
