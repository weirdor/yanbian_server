package org.jeecg.modules.edu.umsTeachingPlan.vo;

import java.util.List;
import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsTeachingPlan;
import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsCourseRecord;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 教学计划
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Data
@ApiModel(value="ums_teaching_planPage对象", description="教学计划")
public class UmsTeachingPlanPage {

	/**主键*/
	@ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**教师ID*/
	@Excel(name = "教师ID", width = 15)
	@ApiModelProperty(value = "教师ID")
    private String teacherId;
	/**课程ID*/
	@Excel(name = "课程ID", width = 15)
	@ApiModelProperty(value = "课程ID")
    private String courseId;
	/**教师姓名*/
	@Excel(name = "教师姓名", width = 15)
	@ApiModelProperty(value = "教师姓名")
    private String teacherName;
	/**课程名称*/
	@Excel(name = "课程名称", width = 15)
	@ApiModelProperty(value = "课程名称")
    private String courseName;
	/**学生ID*/
	@Excel(name = "学生ID", width = 15)
	@ApiModelProperty(value = "学生ID")
    private String studentId;
	/**学生姓名*/
	@Excel(name = "学生姓名", width = 15)
	@ApiModelProperty(value = "学生姓名")
    private String studentName;
	/**总课时*/
	@Excel(name = "总课时", width = 15)
	@ApiModelProperty(value = "总课时")
    private Integer totalCourseNum;
	/**剩余课时*/
	@Excel(name = "剩余课时", width = 15)
	@ApiModelProperty(value = "剩余课时")
    private Integer lastCourseNum;
	/**时间*/
	@Excel(name = "时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "时间")
    private Date time;

	@ExcelCollection(name="课程记录表")
	@ApiModelProperty(value = "课程记录表")
	private List<UmsCourseRecord> umsCourseRecordList;

}
