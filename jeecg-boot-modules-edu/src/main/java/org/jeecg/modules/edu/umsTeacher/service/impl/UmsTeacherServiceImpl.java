package org.jeecg.modules.edu.umsTeacher.service.impl;

import org.jeecg.modules.edu.umsTeacher.entity.UmsTeacher;
import org.jeecg.modules.edu.umsTeacher.mapper.UmsTeacherMapper;
import org.jeecg.modules.edu.umsTeacher.service.IUmsTeacherService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 教师表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class UmsTeacherServiceImpl extends ServiceImpl<UmsTeacherMapper, UmsTeacher> implements IUmsTeacherService {

}
