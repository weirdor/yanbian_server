package org.jeecg.modules.edu.umsFloor.service.impl;

import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import org.jeecg.modules.edu.umsFloor.mapper.UmsClassroomMapper;
import org.jeecg.modules.edu.umsFloor.service.IUmsClassroomService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 教室表
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
@Service
public class UmsClassroomServiceImpl extends ServiceImpl<UmsClassroomMapper, UmsClassroom> implements IUmsClassroomService {
	
	@Autowired
	private UmsClassroomMapper umsClassroomMapper;
	
	@Override
	public List<UmsClassroom> selectByMainId(String mainId) {
		return umsClassroomMapper.selectByMainId(mainId);
	}
}
