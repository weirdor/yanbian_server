package org.jeecg.modules.edu.umsUser.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.edu.umsUser.entity.UmsUser;
import org.jeecg.modules.edu.umsUser.mapper.UmsUserMapper;
import org.jeecg.modules.edu.umsUser.parm.LoginParam;
import org.jeecg.modules.edu.umsUser.service.IUmsUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 用户表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class UmsUserServiceImpl extends ServiceImpl<UmsUserMapper, UmsUser> implements IUmsUserService {


    @Override
    public UmsUser loginAuth(LoginParam loginParam) {
        UmsUser user = this.getOne(Wrappers.<UmsUser>lambdaQuery().eq(UmsUser::getUsername, loginParam.getUsername()));
        if (user == null) {
            throw new JeecgBootException("账号不存在");
        }
        return user;
    }

    @Override
    public Result checkUserIsEffective(UmsUser pmsMember) {
        Result<?> result = new Result<Object>();
        //情况1：根据用户信息查询，该用户不存在
        if (pmsMember == null) {
            result.error500("该用户不存在，请注册");
            return result;
        }
        //情况2：根据用户信息查询，该用户已注销
        //update-begin---author:王帅   Date:20200601  for：if条件永远为falsebug------------
        if (CommonConstant.DEL_FLAG_1.equals(pmsMember.getDelFlag())) {
            //update-end---author:王帅   Date:20200601  for：if条件永远为falsebug------------
            result.error500("该用户已注销");
            return result;
        }
        //情况3：根据用户信息查询，该用户已冻结
        if (CommonConstant.USER_FREEZE.equals(pmsMember.getStatus())) {
            result.error500("该用户已冻结");
            return result;
        }
        return result;
    }

}
