package org.jeecg.modules.edu.umsFloor.service.impl;

import org.jeecg.modules.edu.umsFloor.entity.UmsFloor;
import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import org.jeecg.modules.edu.umsFloor.mapper.UmsClassroomMapper;
import org.jeecg.modules.edu.umsFloor.mapper.UmsFloorMapper;
import org.jeecg.modules.edu.umsFloor.service.IUmsFloorService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 教学楼
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
@Service
public class UmsFloorServiceImpl extends ServiceImpl<UmsFloorMapper, UmsFloor> implements IUmsFloorService {

	@Autowired
	private UmsFloorMapper umsFloorMapper;
	@Autowired
	private UmsClassroomMapper umsClassroomMapper;
	
	@Override
	@Transactional
	public void saveMain(UmsFloor umsFloor, List<UmsClassroom> umsClassroomList) {
		umsFloorMapper.insert(umsFloor);
		if(umsClassroomList!=null && umsClassroomList.size()>0) {
			for(UmsClassroom entity:umsClassroomList) {
				//外键设置
				entity.setFloorId(umsFloor.getId());
				umsClassroomMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(UmsFloor umsFloor,List<UmsClassroom> umsClassroomList) {
		umsFloorMapper.updateById(umsFloor);
		
		//1.先删除子表数据
		umsClassroomMapper.deleteByMainId(umsFloor.getId());
		
		//2.子表数据重新插入
		if(umsClassroomList!=null && umsClassroomList.size()>0) {
			for(UmsClassroom entity:umsClassroomList) {
				//外键设置
				entity.setFloorId(umsFloor.getId());
				umsClassroomMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		umsClassroomMapper.deleteByMainId(id);
		umsFloorMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			umsClassroomMapper.deleteByMainId(id.toString());
			umsFloorMapper.deleteById(id);
		}
	}
	
}
