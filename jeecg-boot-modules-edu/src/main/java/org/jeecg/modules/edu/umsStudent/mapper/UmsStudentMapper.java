package org.jeecg.modules.edu.umsStudent.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.umsStudent.entity.UmsStudent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 学生表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface UmsStudentMapper extends BaseMapper<UmsStudent> {

}
