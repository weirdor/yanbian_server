package org.jeecg.modules.edu.umsClassroomRecord.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.umsClassroomRecord.entity.UmsClassroomRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 教室申请记录
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
public interface UmsClassroomRecordMapper extends BaseMapper<UmsClassroomRecord> {

}
