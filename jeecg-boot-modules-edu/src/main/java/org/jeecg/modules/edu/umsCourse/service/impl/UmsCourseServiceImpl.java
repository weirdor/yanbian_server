package org.jeecg.modules.edu.umsCourse.service.impl;

import org.jeecg.modules.edu.umsCourse.entity.UmsCourse;
import org.jeecg.modules.edu.umsCourse.mapper.UmsCourseMapper;
import org.jeecg.modules.edu.umsCourse.service.IUmsCourseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 课程表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class UmsCourseServiceImpl extends ServiceImpl<UmsCourseMapper, UmsCourse> implements IUmsCourseService {

}
