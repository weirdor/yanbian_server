package org.jeecg.modules.edu.umsStudent.service;

import org.jeecg.modules.edu.umsStudent.entity.UmsStudent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 学生表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface IUmsStudentService extends IService<UmsStudent> {

}
