package org.jeecg.modules.edu.umsTeachingPlan.service.impl;

import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsCourseRecord;
import org.jeecg.modules.edu.umsTeachingPlan.mapper.UmsCourseRecordMapper;
import org.jeecg.modules.edu.umsTeachingPlan.service.IUmsCourseRecordService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 课程记录表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class UmsCourseRecordServiceImpl extends ServiceImpl<UmsCourseRecordMapper, UmsCourseRecord> implements IUmsCourseRecordService {
	
	@Autowired
	private UmsCourseRecordMapper umsCourseRecordMapper;
	
	@Override
	public List<UmsCourseRecord> selectByMainId(String mainId) {
		return umsCourseRecordMapper.selectByMainId(mainId);
	}
}
