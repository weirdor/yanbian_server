package org.jeecg.modules.edu.umsFloor.service;

import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import org.jeecg.modules.edu.umsFloor.entity.UmsFloor;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 教学楼
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
public interface IUmsFloorService extends IService<UmsFloor> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(UmsFloor umsFloor,List<UmsClassroom> umsClassroomList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(UmsFloor umsFloor,List<UmsClassroom> umsClassroomList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
