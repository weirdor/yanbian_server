package org.jeecg.modules.edu.umsUser.parm;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Author werdor
 * @Date 2021/9/4 3:37 下午
 **/
@Data
public class UserEditParam implements Serializable {

    @NotBlank(message = "请上传头像")
    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @NotBlank(message = "请填写昵称")
    @Size(min = 1, max = 60, message = "长度超过了限制")
    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    @NotBlank(message = "简介")
    @Size(min = 1, max = 60, message = "长度超过了限制")
    @ApiModelProperty(value = "简介")
    private String description;


}
