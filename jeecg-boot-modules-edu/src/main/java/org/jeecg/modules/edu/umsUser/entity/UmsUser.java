package org.jeecg.modules.edu.umsUser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 用户表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Data
@TableName("ums_user")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ums_user对象", description="用户表")
public class UmsUser implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户id")
    private String id;
	/**用户账户(跟accout一样)*/
	@Excel(name = "用户账户(跟accout一样)", width = 15)
    @ApiModelProperty(value = "用户账户(跟accout一样)")
    private String username;
	/**用户密码（跟pwd）*/
	@Excel(name = "用户密码（跟pwd）", width = 15)
    @ApiModelProperty(value = "用户密码（跟pwd）")
    private String password;
	/**真实姓名*/
	@Excel(name = "真实姓名", width = 15)
    @ApiModelProperty(value = "真实姓名")
    private String realName;
	/**用户备注*/
	@Excel(name = "用户备注", width = 15)
    @ApiModelProperty(value = "用户备注")
    private String mark;
    /**盐*/
    @Excel(name = "盐", width = 15)
    @ApiModelProperty(value = "盐")
    private String salt;
	/**用户昵称*/
	@Excel(name = "用户昵称", width = 15)
    @ApiModelProperty(value = "用户昵称")
    private String nickname;
	/**用户头像*/
	@Excel(name = "用户头像", width = 15)
    @ApiModelProperty(value = "用户头像")
    private String avatar;
	/**手机号码*/
	@Excel(name = "手机号码", width = 15)
    @ApiModelProperty(value = "手机号码")
    private String phone;
	/**添加ip*/
	@Excel(name = "添加ip", width = 15)
    @ApiModelProperty(value = "添加ip")
    private String addIp;
	/**最后一次登录ip*/
	@Excel(name = "最后一次登录ip", width = 15)
    @ApiModelProperty(value = "最后一次登录ip")
    private String lastIp;
	/**1为正常，0为禁止*/
	@Excel(name = "1为正常，0为禁止", width = 15)
    @ApiModelProperty(value = "1为正常，0为禁止")
    private Integer status;
	/**用户登陆类型，h5,wechat,routine*/
	@Excel(name = "用户登陆类型，h5,wechat,routine", width = 15)
    @ApiModelProperty(value = "用户登陆类型，h5,wechat,routine")
    private String loginType;
	/**微信小程序ID*/
	@Excel(name = "微信小程序ID", width = 15)
    @ApiModelProperty(value = "微信小程序ID")
    private String routineOpenid;
	/**微信unionId*/
	@Excel(name = "微信unionId", width = 15)
    @ApiModelProperty(value = "微信unionId")
    private String unionId;
	/**delFlag*/
	@Excel(name = "delFlag", width = 15)
    @ApiModelProperty(value = "delFlag")
    private Integer delFlag;
	/**最后一次登录时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "最后一次登录时间")
    private Date updateTime;
	/**添加时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "添加时间")
    private Date createTime;
}
