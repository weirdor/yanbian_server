package org.jeecg.modules.edu.umsClassroomRecord.service;

import org.jeecg.modules.edu.umsClassroomRecord.entity.UmsClassroomRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 教室申请记录
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
public interface IUmsClassroomRecordService extends IService<UmsClassroomRecord> {

}
