package org.jeecg.modules.edu.umsFloor.mapper;

import java.util.List;
import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 教室表
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
public interface UmsClassroomMapper extends BaseMapper<UmsClassroom> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<UmsClassroom> selectByMainId(@Param("mainId") String mainId);
}
