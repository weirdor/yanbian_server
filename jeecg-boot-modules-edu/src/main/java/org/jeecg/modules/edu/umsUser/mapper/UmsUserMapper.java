package org.jeecg.modules.edu.umsUser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.umsUser.entity.UmsUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 用户表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface UmsUserMapper extends BaseMapper<UmsUser> {

}
