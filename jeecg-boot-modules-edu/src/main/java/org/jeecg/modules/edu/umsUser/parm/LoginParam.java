package org.jeecg.modules.edu.umsUser.parm;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author werdor
 * @Date 2021/11/14 8:49 下午
 **/
@Data
public class LoginParam {

    /**
     * 用户名
     */
    @NotBlank(message = "用户名参数缺失")
    private String username;

    /**
     * 密码
     */
    private String password;
}
