package org.jeecg.modules.edu.umsTeacher.service;

import org.jeecg.modules.edu.umsTeacher.entity.UmsTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 教师表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface IUmsTeacherService extends IService<UmsTeacher> {

}
