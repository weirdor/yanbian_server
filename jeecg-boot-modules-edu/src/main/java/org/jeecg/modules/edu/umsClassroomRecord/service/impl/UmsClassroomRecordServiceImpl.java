package org.jeecg.modules.edu.umsClassroomRecord.service.impl;

import org.jeecg.modules.edu.umsClassroomRecord.entity.UmsClassroomRecord;
import org.jeecg.modules.edu.umsClassroomRecord.mapper.UmsClassroomRecordMapper;
import org.jeecg.modules.edu.umsClassroomRecord.service.IUmsClassroomRecordService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 教室申请记录
 * @Author: jeecg-boot
 * @Date:   2021-12-11
 * @Version: V1.0
 */
@Service
public class UmsClassroomRecordServiceImpl extends ServiceImpl<UmsClassroomRecordMapper, UmsClassroomRecord> implements IUmsClassroomRecordService {

}
