package org.jeecg.modules.edu.umsTeachingPlan.service;

import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsCourseRecord;
import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsTeachingPlan;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 教学计划
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface IUmsTeachingPlanService extends IService<UmsTeachingPlan> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(UmsTeachingPlan umsTeachingPlan,List<UmsCourseRecord> umsCourseRecordList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(UmsTeachingPlan umsTeachingPlan,List<UmsCourseRecord> umsCourseRecordList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
