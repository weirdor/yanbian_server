package org.jeecg.modules.edu.umsTeachingPlan.service;

import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsCourseRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 课程记录表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface IUmsCourseRecordService extends IService<UmsCourseRecord> {

	public List<UmsCourseRecord> selectByMainId(String mainId);
}
