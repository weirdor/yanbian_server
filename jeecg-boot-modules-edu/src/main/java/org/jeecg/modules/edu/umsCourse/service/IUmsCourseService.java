package org.jeecg.modules.edu.umsCourse.service;

import org.jeecg.modules.edu.umsCourse.entity.UmsCourse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 课程表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface IUmsCourseService extends IService<UmsCourse> {

}
