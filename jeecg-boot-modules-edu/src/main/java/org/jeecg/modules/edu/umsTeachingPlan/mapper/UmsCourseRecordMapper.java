package org.jeecg.modules.edu.umsTeachingPlan.mapper;

import java.util.List;
import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsCourseRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 课程记录表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface UmsCourseRecordMapper extends BaseMapper<UmsCourseRecord> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<UmsCourseRecord> selectByMainId(@Param("mainId") String mainId);
}
