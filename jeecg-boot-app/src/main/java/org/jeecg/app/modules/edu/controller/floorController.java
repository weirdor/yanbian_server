package org.jeecg.app.modules.edu.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.modules.edu.umsClassroomRecord.entity.UmsClassroomRecord;
import org.jeecg.modules.edu.umsClassroomRecord.service.IUmsClassroomRecordService;
import org.jeecg.modules.edu.umsClassroomRule.entity.UmsClassroomRule;
import org.jeecg.modules.edu.umsClassroomRule.service.IUmsClassroomRuleService;
import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import org.jeecg.modules.edu.umsFloor.entity.UmsFloor;
import org.jeecg.modules.edu.umsFloor.service.IUmsClassroomService;
import org.jeecg.modules.edu.umsFloor.service.IUmsFloorService;
import org.jeecg.modules.edu.umsFloor.vo.UmsFloorPage;
import org.jeecg.modules.edu.umsUser.entity.UmsUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author werdor
 * @Date 2021/12/11 11:04 上午
 **/
@RestController
@RequestMapping("/floor")
@Slf4j
public class floorController {

    private final IUmsFloorService umsFloorService;

    private final IUmsClassroomRecordService umsClassroomRecordService;

    public final IUmsClassroomService checkClassroomService;

    private final IUmsClassroomRuleService umsClassroomRuleService;

    public floorController(IUmsFloorService umsFloorService, IUmsClassroomRecordService umsClassroomRecordService, IUmsClassroomService checkClassroomService, IUmsClassroomRuleService umsClassroomRuleService) {
        this.umsFloorService = umsFloorService;
        this.umsClassroomRecordService = umsClassroomRecordService;
        this.checkClassroomService = checkClassroomService;
        this.umsClassroomRuleService = umsClassroomRuleService;
    }

    /**
     * 获取列表
     * @return
     */
    @GetMapping("/list")
    public Result<?> list() {
        List<UmsFloor> list = umsFloorService.list();
        return Result.OK("获取成功", list);
    }


    /**
     * 根据楼层获取教室
     */
    @GetMapping("/listByFloor")
    public Result<?> listByFloor(@RequestParam(name="id",required=true) String id) {
        UmsClassroomRecord classroomRecord = umsClassroomRecordService.getById(id);
        String now = classroomRecord.getStartTime();
        String format = "HH:mm:ss";
        List<String> list = new ArrayList<>();
        //查询记录
        List<UmsClassroomRecord> classroomRecordList = umsClassroomRecordService.list(new LambdaQueryWrapper<UmsClassroomRecord>().eq(UmsClassroomRecord::getTime,classroomRecord.getTime()).ne(UmsClassroomRecord::getId,id).eq(UmsClassroomRecord::getStatus,"1"));
        classroomRecordList.forEach(o->{
           // o.getTime()
            if (classroomRecord.getTime().getTime()==o.getTime().getTime()){
                Date nowTime;
                try {
                    nowTime = new SimpleDateFormat(format).parse(now);
                    Date startTime = DateUtil.parse(o.getStartTime(), format);
                    Date endTime = DateUtil.parse(o.getEndTime(), format);
                    if (isEffectiveDate(nowTime, startTime, endTime)) {
                        //获取教室ID
                        list.add(o.getClassroomId());
                    }
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        List<UmsClassroom> classroomList = checkClassroomService.list(new LambdaQueryWrapper<UmsClassroom>().eq(UmsClassroom::getFloorId,classroomRecord.getFloorId()));
        List<UmsClassroom> umsClassroomList = new ArrayList<>();
        classroomList.forEach(k->{

            if (list.size()>0){
                list.forEach(i->{
                    System.out.println(k.getId());
                    System.out.println(i);
                    if (!k.getId().equals(i)){
                        umsClassroomList.add(k);
                    }
                });
            }else {
                umsClassroomList.add(k);
            }


        });
        //查询规则
        List<UmsClassroomRule> umsClassroomRuleList = umsClassroomRuleService.list(new LambdaQueryWrapper<UmsClassroomRule>().eq(UmsClassroomRule::getFloorId,classroomRecord.getFloorId()));
        String formatDate = DateUtil.formatDate(classroomRecord.getTime());
        String dates =  formatDate+" "+now;
        List<String> lists = new ArrayList<>();
        umsClassroomRuleList.forEach(k->{
            Date date = DateUtil.parse(dates, "yyyy-MM-dd HH:mm:ss");
            if (isEffectiveDate(date, k.getStartTime(), k.getEndTime())) {
                //获取教室ID
                lists.add(k.getClassroomId());
            }
        });
        List<UmsClassroom> umsClassroomLists = new ArrayList<>();
        umsClassroomList.forEach(k->{

                    if (lists.size()>0){
                        lists.forEach(i->{
                            System.out.println(i);
                            if (!k.getId().equals(i)){
                                umsClassroomLists.add(k);
                            }
                        });
                    }else {
                        umsClassroomLists.add(k);
                    }


        });

        return Result.OK("获取成功", umsClassroomLists);
    }

    /**
     * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
     *
     * @param nowTime 当前时间
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     * @author jqlin
     */
    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }


    public String judge(Date date1, Date date2, Date startTime, Date endDate) {
        if(date1 == null || date2 == null || startTime == null || endDate == null)
            return "有的数据指空，无法计算。";

        long d1 = date1.getTime();
        long d2 = date2.getTime();
        long v = d2-d1;
        long start = startTime.getTime();
        long end = endDate.getTime();
        if (((d1 - start) >0) && ((end - d2) >0))
            return "date1-date2 在起始至结束的区间内";
        else if ( v > 7200000)  //2小时等于 7200000毫秒
            return "date1-date2 不在起始至结束的区间内，但 这个时间段大于2个小时 ";

        return "date1-date2 不在起始至结束的区间内，并且 这个时间段小于2个小时";
    }


}
