package org.jeecg.app.modules.edu.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.edu.umsClassroomRecord.entity.UmsClassroomRecord;
import org.jeecg.modules.edu.umsClassroomRecord.service.IUmsClassroomRecordService;
import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import org.jeecg.modules.edu.umsFloor.service.IUmsClassroomService;
import org.jeecg.modules.edu.umsUser.entity.UmsUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
* @Description: 教室申请记录
* @Author: jeecg-boot
* @Date:   2021-12-11
* @Version: V1.0
*/
@RestController
@RequestMapping("/umsClassroomRecord")
@Slf4j
public class UmsClassroomRecordController extends JeecgController<UmsClassroomRecord, IUmsClassroomRecordService> {
   @Autowired
   private IUmsClassroomRecordService umsClassroomRecordService;

    @Autowired
    private IUmsClassroomService checkClassroomService;

   /**
    * 分页列表查询
    *
    * @param umsClassroomRecord
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "教室申请记录-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsClassroomRecord umsClassroomRecord,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsClassroomRecord> queryWrapper = QueryGenerator.initQueryWrapper(umsClassroomRecord, req.getParameterMap());
       UmsUser pmsMember = (UmsUser) StpUtil.getSession().get("umsUser");
       if (pmsMember.getLoginType().equals("1")){
           queryWrapper.eq("user_id",pmsMember.getId());
       }
       Page<UmsClassroomRecord> page = new Page<UmsClassroomRecord>(pageNo, pageSize);
       IPage<UmsClassroomRecord> pageList = umsClassroomRecordService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsClassroomRecord
    * @return
    */
   @AutoLog(value = "教室申请记录-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsClassroomRecord umsClassroomRecord) {
       UmsUser pmsMember = (UmsUser) StpUtil.getSession().get("umsUser");
       umsClassroomRecord.setUserId(pmsMember.getId());
       umsClassroomRecord.setCreateTime(new Date());
       umsClassroomRecord.setUpdateTime(new Date());
       umsClassroomRecord.setCreateBy(pmsMember.getRealName());
       umsClassroomRecord.setStatus("0");
       umsClassroomRecordService.save(umsClassroomRecord);
       return Result.OK("添加成功！");
   }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "教室申请记录-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
        UmsClassroomRecord umsClassroomRecord = umsClassroomRecordService.getById(id);
        if (umsClassroomRecord.getClassroomId()!=null){
            UmsClassroom classroomClassroom = checkClassroomService.getById(umsClassroomRecord.getClassroomId());
            umsClassroomRecord.setClassroomName(classroomClassroom.getName());
        }
        if(umsClassroomRecord==null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(umsClassroomRecord);
    }


    /**
     *  编辑
     *
     * @param umsClassroomRecord
     * @return
     */
    @AutoLog(value = "教室申请记录-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody UmsClassroomRecord umsClassroomRecord) {
        UmsUser pmsMember = (UmsUser) StpUtil.getSession().get("umsUser");
        umsClassroomRecord.setCheckId(pmsMember.getId());
        umsClassroomRecord.setStatus(umsClassroomRecord.getStatus());
        umsClassroomRecordService.updateById(umsClassroomRecord);
        return Result.OK("编辑成功!");
    }

}
