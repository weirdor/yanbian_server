package org.jeecg.admin.modules.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import org.jeecg.modules.edu.umsFloor.entity.UmsFloor;
import org.jeecg.modules.edu.umsFloor.service.IUmsClassroomService;
import org.jeecg.modules.edu.umsFloor.service.IUmsFloorService;
import org.jeecg.modules.edu.umsFloor.vo.UmsFloorPage;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
* @Description: 教学楼
* @Author: jeecg-boot
* @Date:   2021-12-11
* @Version: V1.0
*/
@Api(tags="教学楼")
@RestController
@RequestMapping("/umsFloor/umsFloor")
@Slf4j
public class UmsFloorController {
   @Autowired
   private IUmsFloorService umsFloorService;
   @Autowired
   private IUmsClassroomService umsClassroomService;

   /**
    * 分页列表查询
    *
    * @param umsFloor
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "教学楼-分页列表查询")
   @ApiOperation(value="教学楼-分页列表查询", notes="教学楼-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsFloor umsFloor,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsFloor> queryWrapper = QueryGenerator.initQueryWrapper(umsFloor, req.getParameterMap());
       Page<UmsFloor> page = new Page<UmsFloor>(pageNo, pageSize);
       IPage<UmsFloor> pageList = umsFloorService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsFloorPage
    * @return
    */
   @AutoLog(value = "教学楼-添加")
   @ApiOperation(value="教学楼-添加", notes="教学楼-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsFloorPage umsFloorPage) {
       UmsFloor umsFloor = new UmsFloor();
       BeanUtils.copyProperties(umsFloorPage, umsFloor);
       umsFloorService.saveMain(umsFloor, umsFloorPage.getUmsClassroomList());
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsFloorPage
    * @return
    */
   @AutoLog(value = "教学楼-编辑")
   @ApiOperation(value="教学楼-编辑", notes="教学楼-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsFloorPage umsFloorPage) {
       UmsFloor umsFloor = new UmsFloor();
       BeanUtils.copyProperties(umsFloorPage, umsFloor);
       UmsFloor umsFloorEntity = umsFloorService.getById(umsFloor.getId());
       if(umsFloorEntity==null) {
           return Result.error("未找到对应数据");
       }
       umsFloorService.updateMain(umsFloor, umsFloorPage.getUmsClassroomList());
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教学楼-通过id删除")
   @ApiOperation(value="教学楼-通过id删除", notes="教学楼-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsFloorService.delMain(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "教学楼-批量删除")
   @ApiOperation(value="教学楼-批量删除", notes="教学楼-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsFloorService.delBatchMain(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功！");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教学楼-通过id查询")
   @ApiOperation(value="教学楼-通过id查询", notes="教学楼-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsFloor umsFloor = umsFloorService.getById(id);
       if(umsFloor==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsFloor);

   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教室表通过主表ID查询")
   @ApiOperation(value="教室表主表ID查询", notes="教室表-通主表ID查询")
   @GetMapping(value = "/queryUmsClassroomByMainId")
   public Result<?> queryUmsClassroomListByMainId(@RequestParam(name="id",required=true) String id) {
       List<UmsClassroom> umsClassroomList = umsClassroomService.selectByMainId(id);
       return Result.OK(umsClassroomList);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsFloor
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsFloor umsFloor) {
     // Step.1 组装查询条件查询数据
     QueryWrapper<UmsFloor> queryWrapper = QueryGenerator.initQueryWrapper(umsFloor, request.getParameterMap());
     LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

     //Step.2 获取导出数据
     List<UmsFloor> queryList = umsFloorService.list(queryWrapper);
     // 过滤选中数据
     String selections = request.getParameter("selections");
     List<UmsFloor> umsFloorList = new ArrayList<UmsFloor>();
     if(oConvertUtils.isEmpty(selections)) {
         umsFloorList = queryList;
     }else {
         List<String> selectionList = Arrays.asList(selections.split(","));
         umsFloorList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
     }

     // Step.3 组装pageList
     List<UmsFloorPage> pageList = new ArrayList<UmsFloorPage>();
     for (UmsFloor main : umsFloorList) {
         UmsFloorPage vo = new UmsFloorPage();
         BeanUtils.copyProperties(main, vo);
         List<UmsClassroom> umsClassroomList = umsClassroomService.selectByMainId(main.getId());
         vo.setUmsClassroomList(umsClassroomList);
         pageList.add(vo);
     }

     // Step.4 AutoPoi 导出Excel
     ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
     mv.addObject(NormalExcelConstants.FILE_NAME, "教学楼列表");
     mv.addObject(NormalExcelConstants.CLASS, UmsFloorPage.class);
     mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("教学楼数据", "导出人:"+sysUser.getRealname(), "教学楼"));
     mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
     return mv;
   }

   /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
     MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
     Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
     for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
         MultipartFile file = entity.getValue();// 获取上传文件对象
         ImportParams params = new ImportParams();
         params.setTitleRows(2);
         params.setHeadRows(1);
         params.setNeedSave(true);
         try {
             List<UmsFloorPage> list = ExcelImportUtil.importExcel(file.getInputStream(), UmsFloorPage.class, params);
             for (UmsFloorPage page : list) {
                 UmsFloor po = new UmsFloor();
                 BeanUtils.copyProperties(page, po);
                 umsFloorService.saveMain(po, page.getUmsClassroomList());
             }
             return Result.OK("文件导入成功！数据行数:" + list.size());
         } catch (Exception e) {
             log.error(e.getMessage(),e);
             return Result.error("文件导入失败:"+e.getMessage());
         } finally {
             try {
                 file.getInputStream().close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
     }
     return Result.OK("文件导入失败！");
   }

}
