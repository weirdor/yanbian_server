package org.jeecg.admin.modules.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.edu.umsClassroomRecord.entity.UmsClassroomRecord;
import org.jeecg.modules.edu.umsClassroomRecord.service.IUmsClassroomRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: 教室申请记录
* @Author: jeecg-boot
* @Date:   2021-12-11
* @Version: V1.0
*/
@Api(tags="教室申请记录")
@RestController
@RequestMapping("/umsClassroomRecord/umsClassroomRecord")
@Slf4j
public class UmsClassroomRecordController extends JeecgController<UmsClassroomRecord, IUmsClassroomRecordService> {
   @Autowired
   private IUmsClassroomRecordService umsClassroomRecordService;

   /**
    * 分页列表查询
    *
    * @param umsClassroomRecord
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "教室申请记录-分页列表查询")
   @ApiOperation(value="教室申请记录-分页列表查询", notes="教室申请记录-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsClassroomRecord umsClassroomRecord,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsClassroomRecord> queryWrapper = QueryGenerator.initQueryWrapper(umsClassroomRecord, req.getParameterMap());
       Page<UmsClassroomRecord> page = new Page<UmsClassroomRecord>(pageNo, pageSize);
       IPage<UmsClassroomRecord> pageList = umsClassroomRecordService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsClassroomRecord
    * @return
    */
   @AutoLog(value = "教室申请记录-添加")
   @ApiOperation(value="教室申请记录-添加", notes="教室申请记录-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsClassroomRecord umsClassroomRecord) {
       umsClassroomRecordService.save(umsClassroomRecord);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsClassroomRecord
    * @return
    */
   @AutoLog(value = "教室申请记录-编辑")
   @ApiOperation(value="教室申请记录-编辑", notes="教室申请记录-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsClassroomRecord umsClassroomRecord) {
       umsClassroomRecordService.updateById(umsClassroomRecord);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教室申请记录-通过id删除")
   @ApiOperation(value="教室申请记录-通过id删除", notes="教室申请记录-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsClassroomRecordService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "教室申请记录-批量删除")
   @ApiOperation(value="教室申请记录-批量删除", notes="教室申请记录-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsClassroomRecordService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教室申请记录-通过id查询")
   @ApiOperation(value="教室申请记录-通过id查询", notes="教室申请记录-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsClassroomRecord umsClassroomRecord = umsClassroomRecordService.getById(id);
       if(umsClassroomRecord==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsClassroomRecord);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsClassroomRecord
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsClassroomRecord umsClassroomRecord) {
       return super.exportXls(request, umsClassroomRecord, UmsClassroomRecord.class, "教室申请记录");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, UmsClassroomRecord.class);
   }

}
