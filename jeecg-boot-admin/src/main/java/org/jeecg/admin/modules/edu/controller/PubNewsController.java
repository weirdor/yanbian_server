package org.jeecg.admin.modules.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.edu.pubNews.entity.PubNews;
import org.jeecg.modules.edu.pubNews.service.IPubNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: 新闻表
* @Author: jeecg-boot
* @Date:   2021-12-09
* @Version: V1.0
*/
@Api(tags="新闻表")
@RestController
@RequestMapping("/pubNews/pubNews")
@Slf4j
public class PubNewsController extends JeecgController<PubNews, IPubNewsService> {
   @Autowired
   private IPubNewsService pubNewsService;

   /**
    * 分页列表查询
    *
    * @param pubNews
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "新闻表-分页列表查询")
   @ApiOperation(value="新闻表-分页列表查询", notes="新闻表-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(PubNews pubNews,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<PubNews> queryWrapper = QueryGenerator.initQueryWrapper(pubNews, req.getParameterMap());
       Page<PubNews> page = new Page<PubNews>(pageNo, pageSize);
       IPage<PubNews> pageList = pubNewsService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param pubNews
    * @return
    */
   @AutoLog(value = "新闻表-添加")
   @ApiOperation(value="新闻表-添加", notes="新闻表-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody PubNews pubNews) {
       pubNewsService.save(pubNews);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param pubNews
    * @return
    */
   @AutoLog(value = "新闻表-编辑")
   @ApiOperation(value="新闻表-编辑", notes="新闻表-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody PubNews pubNews) {
       pubNewsService.updateById(pubNews);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "新闻表-通过id删除")
   @ApiOperation(value="新闻表-通过id删除", notes="新闻表-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       pubNewsService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "新闻表-批量删除")
   @ApiOperation(value="新闻表-批量删除", notes="新闻表-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.pubNewsService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "新闻表-通过id查询")
   @ApiOperation(value="新闻表-通过id查询", notes="新闻表-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       PubNews pubNews = pubNewsService.getById(id);
       if(pubNews==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(pubNews);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param pubNews
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, PubNews pubNews) {
       return super.exportXls(request, pubNews, PubNews.class, "新闻表");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, PubNews.class);
   }

}
