package org.jeecg.admin.modules.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsCourseRecord;
import org.jeecg.modules.edu.umsTeachingPlan.entity.UmsTeachingPlan;
import org.jeecg.modules.edu.umsTeachingPlan.service.IUmsCourseRecordService;
import org.jeecg.modules.edu.umsTeachingPlan.service.IUmsTeachingPlanService;
import org.jeecg.modules.edu.umsTeachingPlan.vo.UmsTeachingPlanPage;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
* @Description: 教学计划
* @Author: jeecg-boot
* @Date:   2021-12-09
* @Version: V1.0
*/
@Api(tags="教学计划")
@RestController
@RequestMapping("/umsTeachingPlan/umsTeachingPlan")
@Slf4j
public class UmsTeachingPlanController {
   @Autowired
   private IUmsTeachingPlanService umsTeachingPlanService;
   @Autowired
   private IUmsCourseRecordService umsCourseRecordService;

   /**
    * 分页列表查询
    *
    * @param umsTeachingPlan
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "教学计划-分页列表查询")
   @ApiOperation(value="教学计划-分页列表查询", notes="教学计划-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsTeachingPlan umsTeachingPlan,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsTeachingPlan> queryWrapper = QueryGenerator.initQueryWrapper(umsTeachingPlan, req.getParameterMap());
       Page<UmsTeachingPlan> page = new Page<UmsTeachingPlan>(pageNo, pageSize);
       IPage<UmsTeachingPlan> pageList = umsTeachingPlanService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsTeachingPlanPage
    * @return
    */
   @AutoLog(value = "教学计划-添加")
   @ApiOperation(value="教学计划-添加", notes="教学计划-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsTeachingPlanPage umsTeachingPlanPage) {
       UmsTeachingPlan umsTeachingPlan = new UmsTeachingPlan();
       BeanUtils.copyProperties(umsTeachingPlanPage, umsTeachingPlan);
       umsTeachingPlanService.saveMain(umsTeachingPlan, umsTeachingPlanPage.getUmsCourseRecordList());
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsTeachingPlanPage
    * @return
    */
   @AutoLog(value = "教学计划-编辑")
   @ApiOperation(value="教学计划-编辑", notes="教学计划-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsTeachingPlanPage umsTeachingPlanPage) {
       UmsTeachingPlan umsTeachingPlan = new UmsTeachingPlan();
       BeanUtils.copyProperties(umsTeachingPlanPage, umsTeachingPlan);
       UmsTeachingPlan umsTeachingPlanEntity = umsTeachingPlanService.getById(umsTeachingPlan.getId());
       if(umsTeachingPlanEntity==null) {
           return Result.error("未找到对应数据");
       }
       umsTeachingPlanService.updateMain(umsTeachingPlan, umsTeachingPlanPage.getUmsCourseRecordList());
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教学计划-通过id删除")
   @ApiOperation(value="教学计划-通过id删除", notes="教学计划-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsTeachingPlanService.delMain(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "教学计划-批量删除")
   @ApiOperation(value="教学计划-批量删除", notes="教学计划-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsTeachingPlanService.delBatchMain(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功！");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教学计划-通过id查询")
   @ApiOperation(value="教学计划-通过id查询", notes="教学计划-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsTeachingPlan umsTeachingPlan = umsTeachingPlanService.getById(id);
       if(umsTeachingPlan==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsTeachingPlan);

   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "课程记录表通过主表ID查询")
   @ApiOperation(value="课程记录表主表ID查询", notes="课程记录表-通主表ID查询")
   @GetMapping(value = "/queryUmsCourseRecordByMainId")
   public Result<?> queryUmsCourseRecordListByMainId(@RequestParam(name="id",required=true) String id) {
       List<UmsCourseRecord> umsCourseRecordList = umsCourseRecordService.selectByMainId(id);
       return Result.OK(umsCourseRecordList);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsTeachingPlan
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsTeachingPlan umsTeachingPlan) {
     // Step.1 组装查询条件查询数据
     QueryWrapper<UmsTeachingPlan> queryWrapper = QueryGenerator.initQueryWrapper(umsTeachingPlan, request.getParameterMap());
     LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

     //Step.2 获取导出数据
     List<UmsTeachingPlan> queryList = umsTeachingPlanService.list(queryWrapper);
     // 过滤选中数据
     String selections = request.getParameter("selections");
     List<UmsTeachingPlan> umsTeachingPlanList = new ArrayList<UmsTeachingPlan>();
     if(oConvertUtils.isEmpty(selections)) {
         umsTeachingPlanList = queryList;
     }else {
         List<String> selectionList = Arrays.asList(selections.split(","));
         umsTeachingPlanList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
     }

     // Step.3 组装pageList
     List<UmsTeachingPlanPage> pageList = new ArrayList<UmsTeachingPlanPage>();
     for (UmsTeachingPlan main : umsTeachingPlanList) {
         UmsTeachingPlanPage vo = new UmsTeachingPlanPage();
         BeanUtils.copyProperties(main, vo);
         List<UmsCourseRecord> umsCourseRecordList = umsCourseRecordService.selectByMainId(main.getId());
         vo.setUmsCourseRecordList(umsCourseRecordList);
         pageList.add(vo);
     }

     // Step.4 AutoPoi 导出Excel
     ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
     mv.addObject(NormalExcelConstants.FILE_NAME, "教学计划列表");
     mv.addObject(NormalExcelConstants.CLASS, UmsTeachingPlanPage.class);
     mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("教学计划数据", "导出人:"+sysUser.getRealname(), "教学计划"));
     mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
     return mv;
   }

   /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
     MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
     Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
     for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
         MultipartFile file = entity.getValue();// 获取上传文件对象
         ImportParams params = new ImportParams();
         params.setTitleRows(2);
         params.setHeadRows(1);
         params.setNeedSave(true);
         try {
             List<UmsTeachingPlanPage> list = ExcelImportUtil.importExcel(file.getInputStream(), UmsTeachingPlanPage.class, params);
             for (UmsTeachingPlanPage page : list) {
                 UmsTeachingPlan po = new UmsTeachingPlan();
                 BeanUtils.copyProperties(page, po);
                 umsTeachingPlanService.saveMain(po, page.getUmsCourseRecordList());
             }
             return Result.OK("文件导入成功！数据行数:" + list.size());
         } catch (Exception e) {
             log.error(e.getMessage(),e);
             return Result.error("文件导入失败:"+e.getMessage());
         } finally {
             try {
                 file.getInputStream().close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
     }
     return Result.OK("文件导入失败！");
   }

}
