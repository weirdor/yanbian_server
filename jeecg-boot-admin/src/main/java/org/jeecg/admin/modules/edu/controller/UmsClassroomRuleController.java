package org.jeecg.admin.modules.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.edu.umsClassroomRule.entity.UmsClassroomRule;
import org.jeecg.modules.edu.umsClassroomRule.service.IUmsClassroomRuleService;
import org.jeecg.modules.edu.umsFloor.entity.UmsClassroom;
import org.jeecg.modules.edu.umsFloor.entity.UmsFloor;
import org.jeecg.modules.edu.umsFloor.service.IUmsClassroomService;
import org.jeecg.modules.edu.umsFloor.service.IUmsFloorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: 教室规则
* @Author: jeecg-boot
* @Date:   2021-12-12
* @Version: V1.0
*/
@Api(tags="教室规则")
@RestController
@RequestMapping("/umsClassroomRule/umsClassroomRule")
@Slf4j
public class UmsClassroomRuleController extends JeecgController<UmsClassroomRule, IUmsClassroomRuleService> {
   @Autowired
   private IUmsClassroomRuleService umsClassroomRuleService;

    @Autowired
    private IUmsClassroomService classroomService;

   /**
    * 分页列表查询
    *
    * @param umsClassroomRule
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "教室规则-分页列表查询")
   @ApiOperation(value="教室规则-分页列表查询", notes="教室规则-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsClassroomRule umsClassroomRule,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsClassroomRule> queryWrapper = QueryGenerator.initQueryWrapper(umsClassroomRule, req.getParameterMap());
       Page<UmsClassroomRule> page = new Page<UmsClassroomRule>(pageNo, pageSize);
       IPage<UmsClassroomRule> pageList = umsClassroomRuleService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsClassroomRule
    * @return
    */
   @AutoLog(value = "教室规则-添加")
   @ApiOperation(value="教室规则-添加", notes="教室规则-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsClassroomRule umsClassroomRule) {
       UmsClassroom classroom= classroomService.getOne(new LambdaQueryWrapper<UmsClassroom>().eq(UmsClassroom::getId, umsClassroomRule.getClassroomId()));
       umsClassroomRule.setFloorId(classroom.getFloorId());
       umsClassroomRuleService.save(umsClassroomRule);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsClassroomRule
    * @return
    */
   @AutoLog(value = "教室规则-编辑")
   @ApiOperation(value="教室规则-编辑", notes="教室规则-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsClassroomRule umsClassroomRule) {
      // umsClassroomRule.getClassroomId()
       UmsClassroom classroom= classroomService.getOne(new LambdaQueryWrapper<UmsClassroom>().eq(UmsClassroom::getId, umsClassroomRule.getClassroomId()));
       umsClassroomRule.setFloorId(classroom.getFloorId());
       umsClassroomRuleService.updateById(umsClassroomRule);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教室规则-通过id删除")
   @ApiOperation(value="教室规则-通过id删除", notes="教室规则-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsClassroomRuleService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "教室规则-批量删除")
   @ApiOperation(value="教室规则-批量删除", notes="教室规则-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsClassroomRuleService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "教室规则-通过id查询")
   @ApiOperation(value="教室规则-通过id查询", notes="教室规则-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsClassroomRule umsClassroomRule = umsClassroomRuleService.getById(id);
       if(umsClassroomRule==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsClassroomRule);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsClassroomRule
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsClassroomRule umsClassroomRule) {
       return super.exportXls(request, umsClassroomRule, UmsClassroomRule.class, "教室规则");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, UmsClassroomRule.class);
   }

}
