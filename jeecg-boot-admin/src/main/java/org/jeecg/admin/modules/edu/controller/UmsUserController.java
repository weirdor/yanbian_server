package org.jeecg.admin.modules.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.edu.umsUser.entity.UmsUser;
import org.jeecg.modules.edu.umsUser.service.IUmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: 用户表
* @Author: jeecg-boot
* @Date:   2021-12-09
* @Version: V1.0
*/
@Api(tags="用户表")
@RestController
@RequestMapping("/umsUser/umsUser")
@Slf4j
public class UmsUserController extends JeecgController<UmsUser, IUmsUserService> {
   @Autowired
   private IUmsUserService umsUserService;

   /**
    * 分页列表查询
    *
    * @param umsUser
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "用户表-分页列表查询")
   @ApiOperation(value="用户表-分页列表查询", notes="用户表-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsUser umsUser,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsUser> queryWrapper = QueryGenerator.initQueryWrapper(umsUser, req.getParameterMap());
       Page<UmsUser> page = new Page<UmsUser>(pageNo, pageSize);
       IPage<UmsUser> pageList = umsUserService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsUser
    * @return
    */
   @AutoLog(value = "用户表-添加")
   @ApiOperation(value="用户表-添加", notes="用户表-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsUser umsUser) {
       umsUserService.save(umsUser);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsUser
    * @return
    */
   @AutoLog(value = "用户表-编辑")
   @ApiOperation(value="用户表-编辑", notes="用户表-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsUser umsUser) {
       umsUserService.updateById(umsUser);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "用户表-通过id删除")
   @ApiOperation(value="用户表-通过id删除", notes="用户表-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsUserService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "用户表-批量删除")
   @ApiOperation(value="用户表-批量删除", notes="用户表-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsUserService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "用户表-通过id查询")
   @ApiOperation(value="用户表-通过id查询", notes="用户表-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsUser umsUser = umsUserService.getById(id);
       if(umsUser==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsUser);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsUser
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsUser umsUser) {
       return super.exportXls(request, umsUser, UmsUser.class, "用户表");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, UmsUser.class);
   }

}
