package org.jeecg.admin.modules.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.edu.umsCourse.entity.UmsCourse;
import org.jeecg.modules.edu.umsCourse.service.IUmsCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: 课程表
* @Author: jeecg-boot
* @Date:   2021-12-09
* @Version: V1.0
*/
@Api(tags="课程表")
@RestController
@RequestMapping("/umsCourse/umsCourse")
@Slf4j
public class UmsCourseController extends JeecgController<UmsCourse, IUmsCourseService> {
   @Autowired
   private IUmsCourseService umsCourseService;

   /**
    * 分页列表查询
    *
    * @param umsCourse
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "课程表-分页列表查询")
   @ApiOperation(value="课程表-分页列表查询", notes="课程表-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsCourse umsCourse,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsCourse> queryWrapper = QueryGenerator.initQueryWrapper(umsCourse, req.getParameterMap());
       Page<UmsCourse> page = new Page<UmsCourse>(pageNo, pageSize);
       IPage<UmsCourse> pageList = umsCourseService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsCourse
    * @return
    */
   @AutoLog(value = "课程表-添加")
   @ApiOperation(value="课程表-添加", notes="课程表-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsCourse umsCourse) {
       umsCourseService.save(umsCourse);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsCourse
    * @return
    */
   @AutoLog(value = "课程表-编辑")
   @ApiOperation(value="课程表-编辑", notes="课程表-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsCourse umsCourse) {
       umsCourseService.updateById(umsCourse);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "课程表-通过id删除")
   @ApiOperation(value="课程表-通过id删除", notes="课程表-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsCourseService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "课程表-批量删除")
   @ApiOperation(value="课程表-批量删除", notes="课程表-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsCourseService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "课程表-通过id查询")
   @ApiOperation(value="课程表-通过id查询", notes="课程表-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsCourse umsCourse = umsCourseService.getById(id);
       if(umsCourse==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsCourse);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsCourse
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsCourse umsCourse) {
       return super.exportXls(request, umsCourse, UmsCourse.class, "课程表");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, UmsCourse.class);
   }

}
